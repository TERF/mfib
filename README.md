# MFIB Algorithm for Blocking #

Kenig, B., & Gal, A. (2013). Mfiblocks: An effective blocking algorithm for entity resolution. Information Systems, 38(6), 908-926.

In order to install and run the project, please refer to the [wiki][mfibRun]
[mfibRun]: https://bitbucket.org/TERF/mfib/wiki/Home

### Contributors: ###

* Jonathan Svirsky
* Sapir Golan
* Batya Kenig