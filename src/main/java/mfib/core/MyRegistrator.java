package mfib.core;

import com.esotericsoftware.kryo.Kryo;


import mfib.bitsets.EWAH_BitSet_Factory;
import mfib.interfaces.Clearer;
import mfib.interfaces.ClearerFactory;
import mfib.interfaces.IFRecord;
import mfib.interfaces.SetPairIF;

import org.apache.spark.serializer.KryoRegistrator;
/***
 * In order to use Kryo serialization we have to register our classes. (Spark tuning)
 * @author Jonathan Svirsky
 */
public class MyRegistrator implements KryoRegistrator {

	@Override
	public void registerClasses(Kryo kryo) {
		kryo.register(EWAH_BitSet_Factory.class);
		kryo.register(Clearer.class);
		kryo.register(ClearerFactory.class);
		kryo.register(IFRecord.class);
		kryo.register(SetPairIF.class);
		kryo.register(FrequentItem.class);
	}

}