//package mfib.core;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.BitSet;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//import java.util.Set;
//import java.util.Map.Entry;
//
//import org.apache.spark.SparkConf;
//import org.apache.spark.api.java.JavaSparkContext;
//
//import candidateMatches.CandidateMatch;
//import candidateMatches.CandidatePairs;
//import candidateMatches.RecordMatches;
//import mfib.core.BottomUp.MFISetsCheckConfiguration;
//import mfib.enums.DatasetType;
//import mfib.enums.InputFormat;
//import mfib.utilities.SerializationUtilities;
//
///**
// * The main goal is to re-write MFIBlocks so it will be able to be easily run inside other algorithms.
// * Input options: 	(1) get a data and write back from/to hard-disk files
// * 					(2) get a data as a set of objects (in the memory) and return results in the required format
// * 
// * The Main of the MFIBlocking Algorithm
// * @param args
// * The parameters are as follows:
// * 	1. Config - FILES, OBJECTS
// *  0. Config - SPARK or DEFAULT
//	1. Path to the lexicon file created in the previous stage (out parameter 6).
//	2. The dataset of item ids created in the previous stage (out parameter 2).
//	3. The set of min_th parameters you would like the algorithm to run on. This parameter is optional and 0 can be provided as the only parameter effectively eliminating the threshold.
//	4. Path to the generated match file (out parameter 3).
//	5. path to the debug file containing the items themselves (out parameter 7)
//	6. The set of min supports to use.
//	7. Must be set to MFI
//	8. The set of p parameters to use as the Neighberhood Growth constraints.
//	9. Blocks output format [S,Path], [B], [N] (S-statistics+blocks with a path of original CSV, B-only blocks, N-no print)
//	10.Size of the first dataset (optional, only for 2 and more dataset files)
// */
//public class MFIBMain {
//	
//	private final static String FI_DIR = "FIs";
//	private final static String TEMP_RECORD_DIR = "TEMP_RECORD_DIR";
//	private final static File TempDir = new File(TEMP_RECORD_DIR);
//	private final static double MAX_SUPP_CONST = 1.0;//0.005;
//	
//	MFIBContext context; 
//	InputFormat inputFormat;
//	Properties bestParameters;
//	DatasetType dataType;
//	private LexiconCSV lexicon;
//	private static double NG_LIMIT = 3;
//	public static BitSet coveredRecords= null;
//	private static double lastUsedBlockingThreshold;
//	
//	public static void main(String[] args) throws IOException{
//		
//		
//		String inputFormat=(String)SerializationUtilities.loadSerializedObject(args[0]);
//		String datasetType=(String)SerializationUtilities.loadSerializedObject(args[1]);
//		Map<Integer,String[]> records=(Map<Integer,String[]>)SerializationUtilities.loadSerializedObject(args[2]);
//		String[] columnsNames=(String[])SerializationUtilities.loadSerializedObject(args[3]);
//		Map<String,List<Integer>> matches=(Map<String,List<Integer>>)SerializationUtilities.loadSerializedObject(args[4]);
//		String inputPropertiesFile=(String)SerializationUtilities.loadSerializedObject(args[5]);
//		int qGramSize=(int)SerializationUtilities.loadSerializedObject(args[6]);
//		double threshold=(double)SerializationUtilities.loadSerializedObject(args[7]);
//		
//		MFIBMain mfib= new MFIBMain(inputFormat,datasetType);
//		mfib.setConfiguration(datasetType,records,columnsNames,matches,inputPropertiesFile,qGramSize,threshold);
//		ArrayList<Set<Integer>> result=mfib.run();
//		SerializationUtilities.storeSerializedObject(result, "MFIB_RESULT");
//	}
//	public MFIBMain(String inputFormat, String dataType){
//		this.inputFormat=InputFormat.valueOf(inputFormat);
//		this.dataType=DatasetType.valueOf(dataType);
//	}
//
//
//	public ArrayList<Set<Integer>> run() throws IOException{
//		//generate lexicon and numeric records
//		LexiconGenerator lexiconGen=new LexiconGenerator(context);
//		lexicon=lexiconGen.generate();
//		Utilities.globalItemsMap = lexiconGen.getItemsFromLexicon();
//		context.recordSet().enrichRecords(context);
//		NG_LIMIT = context.getBestNGparameter();
//		double minThreshold=context.getBestThreshold();
//		coveredRecords = new BitSet(context.recordSet().getDBsize()+1);
//		//should be : coveredRecords.set(0,true); // no such record - but we start from zero!
//		System.out.println("Running MFI with minimum blocking threshold " + minThreshold +
//				" and NGLimit: " + NG_LIMIT);	
////		long start = System.currentTimeMillis();
//		//obtain all the clusters that has the minimum score
//		CandidatePairs algorithmObtainedPairs = getClustersToUse(minThreshold);
//		ArrayList<Set<Integer>> result = algorithmObtainedPairs.exportToCleanPairs();
//		return result;
//	}
//	
//	private MFIBContext updateArguments (String datasetType, Map<Integer,String[]> records,
//			String[] columnsNames, Map<String, List<Integer>> matches,
//			String inputPropertiesFile, int qGramSize, double threshold) {
//		MFIBContext context = new MFIBContext();
//		context.setDatasetType(datasetType);
//		context.setRecordsFromStr(records);
//		context.setColumns(columnsNames);
//		context.setMatches(matches);
//		context.setPropertiesFile(inputPropertiesFile);
//		context.setQgram(qGramSize);
//		context.setPruning(threshold);
//		return context;
//	}
//
//	private MFIBContext readArguments(String[] args) {
//		MFIBContext context = new MFIBContext();
//		//TODO: check the order of the properties
////		context.setConfiguration(args.get(0));
////		context.setLexiconFile(args.get(1));
////		context.setRecordsFile(args.get(2));
////		context.setMinBlockingThresholds(args.get(3));
////		context.setMatchFile(args.get(4));
////		context.setOrigRecordsFile(args.get(5));
////		context.setMinSup(args.get(6));
////		context.setAlgorithm(Alg.MFI);
////		context.setNGs(args.get(8));
////		context.setFirstDbSize(args);
////		context.setPerformanceFlag(args);
////		context.setPrintFormat(args[9]);
//		return context;
//	}
//	
//	private void loadBestParameters(DatasetType datasetType) throws IOException {
//		bestParameters = new Properties();
//		String filePath="\\data\\mfib\\"+datasetType.name().toString();
//		FileInputStream in = new FileInputStream(filePath);
//		bestParameters.load(in);
//		in.close();
//	}
//	
//	private CandidatePairs getClustersToUse(double minBlockingThreshold){
//		//was -  coveredRecords.set(0,true); // no such record - but we start from 0
//		System.out.println("getClustersToUse... ");
//		int[] minimumSupports = context.getBestMinSups();
//		double[] usedThresholds = new double[minimumSupports.length];
//		//Map<Integer, Record> records = RecordSet.values;
//		File mfiDir = new File(FI_DIR);
//		if(!mfiDir.exists()){
//			if(!mfiDir.mkdir())
//				System.out.println("Failed to create directory " + mfiDir.getAbsolutePath());
//		}
//		
//		CandidatePairs allResults = new CandidatePairs(); //unlimited
//		
//		for(int i=(minimumSupports.length - 1) ; i >=0  && coveredRecords.cardinality() < RecordSet.size; i--){ // array is sorted in ascending order -
//			//begin with largest minSup
//			//continue until all records have been covered OR we have completed running over all minSups			
//			long start = System.currentTimeMillis();
//			//TODO: check content of file
//			File uncoveredRecordsFile = createRecordFileFromRecords(coveredRecords, minimumSupports[i]);	
//			System.out.println("Time to createRecordFileFromRecords" +Double.toString((double)(System.currentTimeMillis()-start)/1000.0));
//			
//			start = System.currentTimeMillis();
//			File mfiFile = Utilities.RunMFIAlg(minimumSupports[i], uncoveredRecordsFile.getAbsolutePath(), mfiDir);
//			//File mfiFile = Utilities.RunPFPGrowth(minimumSupports[i], RecordSet.size-coveredRecords.cardinality(),uncoveredRecordsFile.getAbsolutePath(), mfiDir);
//			System.out.println("Time to run MFI with minsup="+minimumSupports[i] +
//					" on table of size " + (RecordSet.size-coveredRecords.cardinality()) + 
//					" is " + Double.toString((double)(System.currentTimeMillis()-start)/1000.0));
//		
//			start = System.currentTimeMillis();
//			FrequentItemsetContext itemsetContext = createFrequentItemsetContext( mfiFile.getAbsolutePath(), minBlockingThreshold, minimumSupports[i], context);
//			CandidatePairs candidatePairs = null;
//			if (MFISetsCheckConfiguration.SPARK.equals(context.getConfig())) {
//				candidatePairs = SparkBlocksReader.readFIs(itemsetContext);
//			} else {
//				candidatePairs = Utilities.readFIs(itemsetContext);
//			}
//			System.out.println("Time to read MFIs: " + Double.toString((double)(System.currentTimeMillis() - start)/1000.0) + " seconds");
//			
//					
//			start = System.currentTimeMillis();
//			BitMatrix coveragematrix = candidatePairs.exportToBitMatrix();
//			updateCoveredRecords(coveredRecords,coveragematrix.getCoveredRows());
//			System.out.println("Time to updateCoveredRecords " + Double.toString((double)(System.currentTimeMillis() - start)/1000.0) + " seconds");				
//			start = System.currentTimeMillis();				
//			updateCandidatePairs(allResults,candidatePairs );
//			coveragematrix = null; 
//			System.out.println("Time to updateBlockingEfficiency " + Double.toString((double)(System.currentTimeMillis() - start)/1000.0) + " seconds");
//			usedThresholds[i] = candidatePairs.getMinThresh();
//
//			lastUsedBlockingThreshold = candidatePairs.getMinThresh();
//			candidatePairs = null;
//			System.out.println("lastUsedBlockingThreshold: " + lastUsedBlockingThreshold);
//			
//			System.out.println("Number of covered records after running with Minsup=" +
//					minimumSupports[i] +  " is " + coveredRecords.cardinality() + " out of " + RecordSet.size);
//			
//			System.out.println("memory statuses:");
//			//System.out.println("DEBUG: Size of coveredRecords: " + MemoryUtil.deepMemoryUsageOf(coveredRecords,VisibilityFilter.ALL)/Math.pow(2,30) + " GB");
//			//System.out.println("DEBUG: Size of allResults: " + allResults.memoryUsage() + " GB");
//			
//				
//		}
//		System.out.println("Minsups used " + Arrays.toString(minimumSupports));
//		System.out.println("Total number of covered records under minimum blocking threshold " + minBlockingThreshold + 
//				" and minsups " + Arrays.toString(minimumSupports) + " is: " + coveredRecords.cardinality() + " out of " + RecordSet.size + 
//				" which are: " + 100*(coveredRecords.cardinality()/RecordSet.size) + "%");		
//
//		System.out.println("After adding uncovered records: Total number of covered records under blocking threshold " + minBlockingThreshold + 
//				" and minsups " + Arrays.toString(minimumSupports) + " is: " + coveredRecords.cardinality() + " out of " + RecordSet.size + 
//				" which are: " + 100*(coveredRecords.cardinality()/RecordSet.size) + "%");
//		int firstDbSize = context.getFirstDbSize();
//		if (firstDbSize>0) {
//			allResults=removePairsSameSet(allResults,firstDbSize);
//		}
//		
//		return allResults;
//		
//	}
//	
//	private File createRecordFileFromRecords(BitSet coveredRecords, int minSup){		
//		System.out.println("createRecordFileFromRecords... ");
//		File outputFle = null;
//		System.out.println("Directory TempDir= " + TempDir + " TempDir.getAbsolutePath()" + TempDir.getAbsolutePath());
//		try {
//			if(!TempDir.exists()){
//				if(!TempDir.mkdir()) {						
//					System.out.println("failed to create directory " + TempDir.getAbsolutePath());
//				}
//			}
//			outputFle = File.createTempFile("records", null, TempDir);
//			System.out.println("retVal= " + outputFle + " retVal.getAbsolutePath()=" + outputFle.getAbsolutePath());
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}	
//		outputFle.deleteOnExit();
//		if(outputFle.exists()){
//			System.out.println("File " + outputFle.getAbsolutePath() + " exists right after deleteOnExit");
//		}
//		
//		Map<Integer,Integer> appItems = appitems(coveredRecords, minSup);
//		
//		BufferedWriter writer = null;
//		int numOfWrittenLines=0;
//		try {
//			outputFle.delete();
//			outputFle.createNewFile();
//			writer = new BufferedWriter(new FileWriter(outputFle));
//			
//			for(int i=coveredRecords.nextClearBit(0); i>=0 && i <  RecordSet.size ; i=coveredRecords.nextClearBit(i+1)){
//				Record currRecord = context.recordSet().values().get(i);		
//				String toWrite = currRecord.getNumericline(appItems.keySet());
//				writer.write(toWrite);
//				writer.newLine();
//				numOfWrittenLines++;
//			}			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		finally{
//			try {
//				System.out.println("Number of records written: " + numOfWrittenLines);
//				writer.flush();
//				writer.close();		
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		return outputFle;			
//	}
//	
//	private Map<Integer,Integer> appitems(BitSet coveredRecords, int minSup){
//		//System.out.println("appitems... ");
//		Map<Integer,Integer> retVal = new HashMap<Integer, Integer>();
//		for( int i=coveredRecords.nextClearBit(0); i>=0 && i <  RecordSet.size ; i=coveredRecords.nextClearBit(i+1) ){
//			//System.out.println("Record #"+i);
//			//if (context.recordSet().values().keySet().contains(i)){
//				Record currRecord = context.recordSet().values().get(i);		
//				Set<Integer> recordItems = currRecord.getItemsToFrequency().keySet();
//				for (Integer recorditem : recordItems) {
//					//System.out.println("recorditem #"+recorditem);
//					int itemSuppSize = 1;
//					if(retVal.containsKey(recorditem)){
//						itemSuppSize = retVal.get(recorditem) + 1;
//					}
//					retVal.put(recorditem, itemSuppSize);
//				}
//			//}
//			
//		}
//		//System.out.println("appitems... 2");
//		int origSize =  retVal.size();
//		System.out.println("Number of items before pruning too frequent items: " + origSize);
//		double DBSize = RecordSet.size - coveredRecords.cardinality();
//		if(DBSize > 10000){			
//			double removal = ((double)minSup)*DBSize*MAX_SUPP_CONST;
//			Iterator<Entry<Integer,Integer>> retValIterator = retVal.entrySet().iterator();
//			while(retValIterator.hasNext()){
//				Entry<Integer,Integer> currItem =  retValIterator.next();
//				if(currItem.getValue() > removal){
//					retValIterator.remove();
//				}
//			}
//		}
//		System.out.println("Number of items AFTER pruning too frequent items: " + retVal.size());
//		System.out.println("A total of : " + (origSize-retVal.size()) + " items were pruned");
//		return retVal;
//	}
//	
//	private static FrequentItemsetContext createFrequentItemsetContext(
//			String absolutePath, double minBlockingThreshold, int minimumSupport,
//			MFIBContext mfiContext) {
//		FrequentItemsetContext itemsetContext = new FrequentItemsetContext();
//		itemsetContext.setAbsolutePath(absolutePath);
//		itemsetContext.setMinBlockingThreshold(minBlockingThreshold);
//		itemsetContext.setMinimumSupport(minimumSupport);
//		itemsetContext.setMfiContext(mfiContext);
//		//itemsetContext.setGolbalItemsMap(Utilities.globalItemsMap);
//		itemsetContext.setNeiborhoodGrowthLimit(NG_LIMIT);
//		
//		return itemsetContext;
//	}
//	
//	private static void updateCoveredRecords(BitSet coveredRecords, BitSet coveredRows){
//		coveredRecords.or(coveredRows);
//	}
//	
//	private static void updateCandidatePairs(CandidatePairs allResults, final CandidatePairs coveragePairs){	
//		allResults.addAll(coveragePairs);
//		
//	}
//	
//	private static CandidatePairs removePairsSameSet(CandidatePairs actualCPs, int firstDbSize) {
//		System.out.println("Excluding pairs if records from the same set..");
//		CandidatePairs updatedPairs=new CandidatePairs();
//		long start=System.currentTimeMillis();
//		//Set<Set<Integer>> actualPairs=new HashSet<>();
//		for (Entry<Integer,RecordMatches> entry: actualCPs.getAllMatches().entrySet()) { //run over all records
//			for (CandidateMatch cm : entry.getValue().getCandidateMatches()) { //for each record, check out its match
//				if ( 	(entry.getKey()>firstDbSize && cm.getRecordId()>firstDbSize) ||
//						(entry.getKey()<firstDbSize && cm.getRecordId()<firstDbSize)) 
//					continue;
////				if  (entry.getKey()>firstDbSize || cm.getRecordId()>firstDbSize) //for CDDB experiemtns (also add 500 to the command inputs)
////					continue;
//				else 
//					updatedPairs.setPair(entry.getKey(), cm.getRecordId(), actualCPs.getMinThresh());
//				//Set<Integer> temp=new HashSet<Integer>();
//				//temp.add(cm.getRecordId());
//				//temp.add(entry.getKey());
//				//actualPairs.add(temp);
//			}
//		}
//		System.out.println("Time exclude pairs : " + Double.toString((double)(System.currentTimeMillis() - start)/1000.0) + " seconds");
//		return updatedPairs;
//	}
//
//
//	public void setConfiguration(String datasetType, Map<Integer,String[]> records,
//			String[] columnsNames, Map<String, List<Integer>> matches,
//			String inputPropertiesFile, int qGramSize, double threshold) {
//		if (inputFormat==InputFormat.OBJECTS){
//			context = new MFIBContext();
//			context = updateArguments(datasetType,records,columnsNames, matches,
//					inputPropertiesFile, qGramSize, threshold);
//			context.loadBestPropersties();
//			
//		}		
//		else{
//			System.out.println("Wrong setConfiguration method was called! Must include list of objects");
//		}
//	}
//	
//	public void setConfiguration(String[] args) {
//		if (inputFormat==InputFormat.FILES){
//			context = new MFIBContext();
//			context = readArguments(args);
//		}
//		else {
//			System.out.println("Wrong setConfiguration method was called! Must include String[] args");
//		}
//			
//	}
//	
//	
//}
