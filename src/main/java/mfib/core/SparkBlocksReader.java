package mfib.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.transaction.NotSupportedException;
import mfib.bitsets.EWAH_BitSet;
import mfib.interfaces.BitSetIF;
import mfib.interfaces.IFRecord;
import mfib.pools.BitMatrixPool;

import org.apache.hadoop.mapred.InvalidInputException;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import scala.Tuple2;
import candidateMatches.CandidatePairs;
/***
 * Reads frequent item-sets created by fpgrow and marks candidate blocks according to 3 parameters:
 * support condition, ClusterJaccard score, sparse neighborhood condition.
 * Uses SPARK as a local RDD.
 * @author Jonathan Svirsky
 *
 */
public class SparkBlocksReader {

	private final static String ItemsetExpression = "([0-9\\s]+)\\(([0-9]+)\\)$";
	public static int DB_SIZE;
	public static String NEW_LINE = System.getProperty("line.separator");
	static CandidatePairs candidatePairs;
	static Broadcast<Map<Integer, FrequentItem>> broadcastGlobalMap;
	//must be defined on other nodes:
	//public static final String LEXICON_FILE="C:/workspace/mfiblocking/MFIBlocking/MFIblocks/original/exampleInputOutputFiles/DS_800_200_3_1_3_po_clean_lexicon_3grams.txt";
	
	//TODO: change to distributed environment (add option to switch between environments)
	
	/**
	 * Replaces Utilities.readFIs, uses SPARK.
	 * Reads the file of candidate pairs = MFIs produced by MFI algorithm
	 * @param frequentItemsetFile
	 * @param globalItemsMap
	 * @param scoreThreshold
	 * @param records
	 * @param minSup
	 * @param NG_PARAM
	 * @return CandidatePairs object
	 */
	public static CandidatePairs cleanCandidateBlocks(FrequentItemsetContext itemsetContext){
		File mfisFile = new File (itemsetContext.getFrequentItemssetFilePath());
		if ( mfisFile.exists()){
			MFIBContext context = itemsetContext.getMfiContext();
			int minSup = itemsetContext.getMinimumSupport();
			double NG_PARAM = itemsetContext.getNeiborhoodGrowthLimit();
			double scoreThreshold = itemsetContext.getMinBlockingThreshold();
			broadcastGlobalMap = MFIBContext.sc.broadcast(Utilities.globalItemsMap);
			
			resetAtomicIntegerArr(Utilities.clusterScores);	
			StringSimTools.numOfMFIs.set(0);
			StringSimTools.numOfRoughMFIs.set(0);
			StringSimTools.timeInGetClose.set(0);
			StringSimTools.timeInRough.set(0);
			BitMatrixPool.getInstance().restart();
			
			int maxSize = (int) Math.floor(minSup *NG_PARAM );
			candidatePairs = new CandidatePairs(maxSize);
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
			int numOfCores = runtime.availableProcessors();
			JavaRDD<String> fmiSets =MFIBContext.sc.textFile(itemsetContext.getFrequentItemssetFilePath(), numOfCores*3); //JS: Spark tuning: minSplits=numOfCores*3
			JavaRDD<CandidateBlock> parsedBlocks = fmiSets.map(new ParseFILine());
			JavaRDD<CandidateBlock> parsedBlocksWithoutNulls = parsedBlocks.filter(new FilterNullsCandidateBlock());
			JavaPairRDD<CandidateBlock,Double> blocksWithScores = parsedBlocksWithoutNulls.mapToPair( new CalculateScores(context.getLexiconFile(),
					context.getRecordsFile(), context.getOriginalFile(),scoreThreshold , minSup, NG_PARAM));
			JavaPairRDD<CandidateBlock,Double> trueBlocks=blocksWithScores.filter(new TrueBlocks());
//			List<CandidateBlock> itemsets = trueBlocks.keys().collect();
//			for (CandidateBlock block : itemsets){
//				System.out.println(block.items);
//			}
			trueBlocks.count();
		}
		
		return candidatePairs;
	}
	
	
	/**
	 * Imported from Utilities.resetAtomicIntegerArr
	 * @param arr
	 */
	private static void resetAtomicIntegerArr(AtomicInteger[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = new AtomicInteger(0);
		}
	}
	/**
	 * TrueBlocks extends Function<Tuple2<CandidateBlock, Double>, Boolean>
	 * for filtering blocks that holds our 3 conditions.
	 * -100.0: tooLarge in original	
	 * -200.0: scorePruned
	 * -300.0: nonFIs (support.getCardinality() <  minSup) 
	 * -400.0: not classified
	 *
	 */
	static class TrueBlocks implements Function<Tuple2<CandidateBlock, Double>, Boolean>{
		private static final long serialVersionUID = 1L;

		@Override
		public Boolean call(Tuple2<CandidateBlock, Double> tuple)
				throws Exception {
			if (tuple._2!=-100.0 && tuple._2!=-200.0 && tuple._2!=-300.0 &&tuple._2!=-400.0)
				return true;
			return false;
		}	
	}

	static class CalculateScores implements PairFunction<CandidateBlock, CandidateBlock, Double>{
		private static final long serialVersionUID = 1L;
		
		double scoreThreshold;
		int minSup;
		double NG_PARAM;

		public CalculateScores(String lexiconFile, String recordsFile,String origRecordsFile,
				double scoreThreshold, int minSup, double nG_PARAM){
			this.scoreThreshold=scoreThreshold;
			this.minSup=minSup;
			this.NG_PARAM=nG_PARAM;
			
		}
		@Override
		public Tuple2<CandidateBlock, Double> call(CandidateBlock candidateBlock)
				throws Exception {
			// 3.1 support condition 
			if (candidateBlock.supportSize > minSup * NG_PARAM) {
				//tooLarge++;
				return new Tuple2<CandidateBlock, Double>(candidateBlock, -100.0);
			}

			// 3.2 ClusterJaccard score
			List<Integer> currentItemSet = candidateBlock.items;
			double maxClusterScore = StringSimTools.MaxScore(candidateBlock.supportSize, currentItemSet, RecordSet.minRecordLength);
			if (maxClusterScore < 0.1 * scoreThreshold) {
				//scorePruned++;
				return new Tuple2<CandidateBlock, Double>(candidateBlock, -200.0);
			}
			// 3.3 sparse neighborhood condition
			BitSetIF support = null;
			support = getItemsetSupport(currentItemSet);
			if (support.getCardinality() <  minSup) {
				//SparkBlocksReader.nonFIs.incrementAndGet();
				return new Tuple2<CandidateBlock, Double>(candidateBlock, -300.0); // must be the case that the item appears minSup times
				// but in a number of records < minsup
			}
			
			List<IFRecord> FISupportRecords = support.getRecords();
			double currClusterScore = StringSimTools.softTFIDF(
					FISupportRecords, currentItemSet, scoreThreshold);
			FISupportRecords = null;
			Utilities.clusterScores[cellForCluster(currClusterScore)].incrementAndGet();
			if (currClusterScore > scoreThreshold) {
				//SparkBlocksReader.numOfFIs.incrementAndGet();
				support.markPairs(candidatePairs,currClusterScore,candidateBlock.items);
				candidateBlock.addSupport(support.getRecordsIDs());
				return new Tuple2<CandidateBlock, Double>(candidateBlock, currClusterScore);
			}
			//JS:Non of conditions passed and wasn't classified.
			return new Tuple2<CandidateBlock, Double>(candidateBlock, -400.0);
		}
		
		/**
		 * Utilities.getItemsetSupport
		 * @param items
		 * @return
		 */
		public static BitSetIF getItemsetSupport(List<Integer> items) {
			try {
				BitSetIF retVal = new EWAH_BitSet();
				try {
					retVal = retVal.or(broadcastGlobalMap.value().get(items.get(0))
							.getSupport());
					for (int i = 1; i < items.size(); i++) {
						int item = items.get(i);
						BitSetIF itemSupport = broadcastGlobalMap.value().get(item)
								.getSupport();
						retVal = retVal.and(itemSupport);
					}
				} catch (NotSupportedException e) {
					e.printStackTrace();
					return null;
				}
				if (retVal.getCardinality() <= 0) {
					System.out
							.println("DEBUG: itemset with support retVal.getCardinality()"
									+ retVal.getCardinality());
				}
				//time_in_supp_calc.addAndGet(System.currentTimeMillis() - start);
				return retVal;
			} catch (Exception e) {
				System.out.println("cought exception " + e);
				e.printStackTrace();
				return null;
			}
		}	
		private static int cellForCluster(final double score) {
			return (int) Math.ceil(score / 0.05);
		}
	}

	/***
	 * Parser extends Function<String, CandidateBlock> for parsing lines from the text files in SPARK.
	 */
	static class ParseFILine implements Function<String, CandidateBlock> {
		private static final long serialVersionUID = 1L;

		public CandidateBlock call(String line) {
			if (line == null)
				return null;
			if (line.startsWith("(")) // the empty FI - ignore it
				return null;
			line = line.trim();
			List<Integer> retVal = new ArrayList<Integer>();
			Pattern ISPatters = Pattern.compile(ItemsetExpression);
			Matcher fiMatcher = ISPatters.matcher(line);
			boolean matchFound = fiMatcher.find();
			if (!matchFound) {
				//System.out.println("no match found in " + line);
				return null;
			}
			String itemsAsString = fiMatcher.group(1).trim();
			String[] items = itemsAsString.split(" ");
			for (String strItem : items) {
				retVal.add(Integer.parseInt(strItem));
			}
			int supportSize = Integer.parseInt(fiMatcher.group(2).trim());
			CandidateBlock pFI=new CandidateBlock(retVal, supportSize);
			return pFI;
		}
	}
	
	static class FilterNullsCandidateBlock implements Function<CandidateBlock, Boolean> {
		
		private static final long serialVersionUID = 1L;

		@Override
		public Boolean call(CandidateBlock v1) throws Exception {
			if (v1==null) return false;
			return true;
		}
	}
	
	static class DirtyCandidatePairsFromBlock implements Function<CandidateBlock, CandidatePairs> {
		private static final long serialVersionUID = 1L;
		private int maxSize;

		public DirtyCandidatePairsFromBlock(int maxSize) {
			this.maxSize = maxSize;
		}
		public DirtyCandidatePairsFromBlock() {
			this.maxSize = -1;
		}
		
		@Override
		public CandidatePairs call(CandidateBlock v1) throws Exception {
			CandidatePairs result;
			if (maxSize==-1)
				result = new CandidatePairs();
			else 
				result = new CandidatePairs(maxSize);
			BitSetIF support = getItemsetSupport(v1.items);
			support.markPairs(result,1.0,v1.items);
			return result;
		}
		
	}
	static class FilterNullsCandidatePairs implements Function<CandidatePairs, Boolean> {
		
		private static final long serialVersionUID = 1L;

		@Override
		public Boolean call(CandidatePairs v1) throws Exception {
			if (v1==null) return false;
			return true;
		}
	}


	static  class CandidatePairsReducer implements Function2<CandidatePairs, CandidatePairs, CandidatePairs> {
		private static final long serialVersionUID = 1L;
		
		@Override
		public CandidatePairs call(CandidatePairs v1, CandidatePairs v2) throws Exception {
			v1.addAll(v2);
			return v1;
		}

	}

	public static CandidateBlock parseFILine (String line) {
		if (line == null)
			return null;
		if (line.startsWith("(")) // the empty FI - ignore it
			return null;
		line = line.trim();
		List<Integer> retVal = new ArrayList<Integer>();
		Pattern ISPatters = Pattern.compile(ItemsetExpression);
		Matcher fiMatcher = ISPatters.matcher(line);
		boolean matchFound = fiMatcher.find();
		if (!matchFound) {
			//System.out.println("no match found in " + line);
			return null;
		}
		String itemsAsString = fiMatcher.group(1).trim();
		String[] items = itemsAsString.split(" ");
		for (String strItem : items) {
			retVal.add(Integer.parseInt(strItem));
		}
		int supportSize = Integer.parseInt(fiMatcher.group(2).trim());
		CandidateBlock pFI=new CandidateBlock(retVal, supportSize);
		return pFI;
	}
	
	public static CandidatePairs getBlocksNaive(FrequentItemsetContext itemsetContext) {
		System.out.println("Block cleaning disabled. Adding all possible blocks!");
		CandidatePairs result = new CandidatePairs();
		BufferedReader fiReader=null;

		try {
			fiReader = new BufferedReader(new FileReader(new File(itemsetContext.getFrequentItemssetFilePath())));
			System.out.println("About to read MFIs from file "+ itemsetContext.getFrequentItemssetFilePath());
			String currLine = "";
			//stringBuilder.append("Size").append("\t").append("Score").append(Utilities.NEW_LINE);
			int counter=0;
			while (currLine != null) {
				try {
					currLine = fiReader.readLine();
					counter ++;
					if (counter%1000==0) System.out.println(counter + " lines done");
					if (currLine == null)
						break;
					if (currLine.startsWith("(")) // the empty FI - ignore it
						continue;
					CandidateBlock parsedFrequentItems = parseFILine(currLine);
					BitSetIF support = getItemsetSupport(parsedFrequentItems.items);
					support.markPairs(result,1.0,parsedFrequentItems.items);

				} catch (Exception e) {
					System.out.println("Exception occured while parsing line "
							+ currLine);
					e.printStackTrace();
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fiReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public static CandidatePairs getBlocks (FrequentItemsetContext itemsetContext) {
		File mfisFile = new File (itemsetContext.getFrequentItemssetFilePath());
		if ( mfisFile.exists()){
			broadcastGlobalMap = MFIBContext.sc.broadcast(Utilities.globalItemsMap);
		
			System.out.println("Block cleaning disabled. Adding all possible blocks!");
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
			int numOfCores = runtime.availableProcessors();
			
			JavaRDD<String> fmiSets = MFIBContext.sc.textFile(itemsetContext.getFrequentItemssetFilePath(), numOfCores*3); //JS: Spark tuning: minSplits=numOfCores*3
			
			JavaRDD<CandidatePairs> cp=fmiSets.map(new ParseFILine())
					.filter(new FilterNullsCandidateBlock())
					.map(new DirtyCandidatePairsFromBlock())
					.filter(new FilterNullsCandidatePairs());
			if (!cp.isEmpty()) {
				candidatePairs = cp.reduce(new CandidatePairsReducer());
			}
			
		}
		
		
		
		return candidatePairs;
	}
	
	public static void empty(){
		candidatePairs=null;
	}
	
	public static class CandidatePairsFromBlock implements Function<Tuple2<CandidateBlock, Double>, CandidatePairs> {

		private static final long serialVersionUID = 1L;
		private int maxSize;

		public CandidatePairsFromBlock(int maxSize) {
			this.maxSize = maxSize;
		}
		
		public CandidatePairsFromBlock() {
			this.maxSize = -1;
		}
		
		@Override
		public CandidatePairs call(Tuple2<CandidateBlock, Double> tuple)
				throws Exception {
			CandidatePairs result;
			if (maxSize==-1)
				result = new CandidatePairs();
			else 
				result = new CandidatePairs(maxSize);
			
			BitSetIF support = getItemsetSupport(tuple._1.items);
			support.markPairs(result,tuple._2,tuple._1.items);
			
			return result;
		}
	}
	
	/**
	 * Utilities.getItemsetSupport
	 * @param items
	 * @return
	 */
	public static BitSetIF getItemsetSupport(List<Integer> items) {
		try {
			BitSetIF retVal = new EWAH_BitSet();
			try {
				retVal = retVal.or(broadcastGlobalMap.value().get(items.get(0))
						.getSupport());
				for (int i = 1; i < items.size(); i++) {
					int item = items.get(i);
					BitSetIF itemSupport = broadcastGlobalMap.value().get(item)
							.getSupport();
					retVal = retVal.and(itemSupport);
				}
			} catch (NotSupportedException e) {
				e.printStackTrace();
				return null;
			}
			if (retVal.getCardinality() <= 0) {
				System.out
						.println("DEBUG: itemset with support retVal.getCardinality()"
								+ retVal.getCardinality());
			}
			//time_in_supp_calc.addAndGet(System.currentTimeMillis() - start);
			return retVal;
		} catch (Exception e) {
			System.out.println("cought exception " + e);
			e.printStackTrace();
			return null;
		}

	}
	
}