package mfib.core;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Itemset<T> implements Serializable{
	private Set<T> itemset;
	long frequency;
	
	public Itemset(Collection<T> itemSet, long frequency){
		itemset=new HashSet<T>();
		itemset.addAll(itemSet);
		this.frequency=frequency;
	}
	
	public Set<T> getSet(){
		return itemset;
	}
	
	public long frequency(){
		return frequency;
	}
	
	
}
