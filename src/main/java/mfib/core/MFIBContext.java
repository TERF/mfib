package mfib.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import mfib.core.BottomUp.Alg;
import mfib.core.BottomUp.MFISetsCheckConfiguration;

import org.apache.commons.lang.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;



public class MFIBContext {
	

	private double[] minBlockingThresholds;
	private int[] minSup;
	private double[] neighborhoodGrowth;
	private MFISetsCheckConfiguration configuration;
	private String matchFile;
	private Alg alg;
	private String lexiconFile;
	private String recordsFile;
	private String origRecordsFile;
	//private Map<Integer, Record> records;
	private boolean inPerformanceMode;
	private int firstDbSize;
	private String printFormat;
	private String originalRecordsPath;
	//===============new by JS
	//RecordSet recordSet;
	Map<String,List<Integer>> matches;
	String propertiesFile;
	int qgramSize;
	double pruning;
	String numericFileName;
	String cleanStringFileName;
	double bestNGparameter;
	int[] bestMinSups;
	double bestThreshold;
	String datasetType;
	public static JavaSparkContext sc;
	
	public MFIBContext() {
		this.firstDbSize = 0;
		this.configuration = MFISetsCheckConfiguration.DEFAULT;
		//createSparkContext(getConfig());
		
	}
	
	private static void createSparkContext(MFISetsCheckConfiguration config) {
		if (config.equals(MFISetsCheckConfiguration.SPARK)) {
			//System.setProperty("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
			//System.setProperty("spark.kryo.registrator", "fimEntityResolution.MyRegistrator");
			System.setProperty("spark.executor.memory", "2g");
			System.setProperty("hadoop.home.dir", "C:\\workspace\\winutils\\");
			//System.getProperty("spark.akka.askTimeout","50000");
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
			int numOfCores = runtime.availableProcessors();		
			SparkConf conf = new SparkConf();
			conf.setMaster("local["+numOfCores+"]");
			conf.setAppName("MFIBlocks");
			sc=new JavaSparkContext(conf);
			System.out.println("SPARK HOME="+sc.getSparkHome());
		}
	}
	
	public double getBestNGparameter(){
		return bestNGparameter;
	}
	
	public double getBestThreshold(){
		return bestThreshold;
	}
	
	public int[] getBestMinSups(){
		return bestMinSups;
	}
	
	/***
	 * Assumes that there is a file in the working directory with the properties and it's name is as a dataset type
	 * @return
	 */
	public void loadBestPropersties() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			 
			input = new FileInputStream(datasetType);
	 
			// load a properties file
			prop.load(input);
	 
			// get the property value and print it out
			this.bestNGparameter=Double.parseDouble(prop.getProperty("NG"));
			this.bestThreshold=Double.parseDouble(prop.getProperty("TH"));
			String[] minSupsStrings = prop.getProperty("MINSUP").split(",");
			this.bestMinSups=new int[minSupsStrings.length];
			for (int i=0; i< minSupsStrings.length;i++){
				bestMinSups[i]=Integer.parseInt(minSupsStrings[i]);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}



//	public RecordSet recordSet(){
//		return recordSet;
//	}
	
	public void setMatchFile(String matchFile) {
		this.matchFile = matchFile;
	}

	public void setMinSup(String minSup) {
		int[] minSupInt = getInts(minSup);
		Arrays.sort(minSupInt);
		this.minSup = minSupInt;
	}

	public void setMinBlockingThresholds(String minBlockingThresholds) {
		this.minBlockingThresholds = getThresholds(minBlockingThresholds);
		
	}

	public void setAlgorithm(Alg alg) {
		this.alg = alg;
	}

	public void setNGs(String neighborhoodGrowth) {
		this.neighborhoodGrowth = getDoubles(neighborhoodGrowth);
		
	}

	public void setLexiconFile(String lexiconFile) {
		this.lexiconFile = lexiconFile;
	}

	public void setRecordsFile(String recordsFile) {
		this.recordsFile = recordsFile;
	}

	public void setOrigRecordsFile(String origRecordsFile) {
		this.origRecordsFile = origRecordsFile;
		
	}

//	public void setRecords(Map<Integer, Record> records) {
//		this.records = records;
//	}
	
	public void setPerformanceFlag(String[] args) {
		this.inPerformanceMode = false;
		int lastArgument = args.length - 1;
		if ("perf".equalsIgnoreCase(args[lastArgument])) {
			this.inPerformanceMode = true;
		}
	}

	public String getLexiconFile() {
		return lexiconFile;
	}

	public String getRecordsFile() {
		return recordsFile;
	}
	
	public String getOriginalFile() {
		return origRecordsFile;
	}
	
	private double[] getThresholds(String strDoubles){
		String[] thresholds = strDoubles.split(",");
		double[] dThresholds = new double[thresholds.length];
		for(int i=0 ; i < thresholds.length ; i++ ){
			dThresholds[i] = Double.parseDouble(thresholds[i].trim());
		}
		return dThresholds;
	}
	
	private int[] getInts(String strInts){
		String[] intStrs = strInts.trim().split(",");
		int[] ints = new int[intStrs.length];
		for(int i=0 ; i < intStrs.length ; i++ ){
			ints[i] = Integer.parseInt(intStrs[i].trim());
		}
		return ints;
	}

	private  double[] getDoubles(String strDbs){
		String[] dbStrs = strDbs.trim().split(",");
		double[] dbs = new double[dbStrs.length];
		for(int i=0 ; i < dbStrs.length ; i++ ){
			dbs[i] = Double.parseDouble(dbStrs[i].trim());
		}
		return dbs;
	}

	public double[] getMinBlockingThresholds() {
		return this.minBlockingThresholds;
	}

	public String getAlgName() {
		return alg.toString();
	}

	public double[] getNeighborhoodGrowth() {
		return neighborhoodGrowth;
	}

	public int[] getMinSup() {
		return this.minSup;
	}

	public String getMatchFile() {
		return this.matchFile;
	}

//	public int getRecordsSize() {
//		return this.records.size();
//	}

	public MFISetsCheckConfiguration getConfig() {
		return this.configuration;
	}

//	public Map<Integer, Record> getRecords() {
//		return this.records;
//	}

	public boolean isInPerformanceMode() {
		return this.inPerformanceMode;
	}

	public void setFirstDbSize(String[] args) {
		if(args.length > 10 && args[10] != null){
			if (StringUtils.isNumeric(args[10]) ) {
				this.firstDbSize = Integer.parseInt(args[10]);
			}
		}
	}

	public int getFirstDbSize() {
		return firstDbSize;
	}

	public void setConfiguration(String configuration) {
		try {
			this.configuration = MFISetsCheckConfiguration.valueOf(configuration);
		} catch (Exception e) {
			System.err.println(String.format("Failed to read value of configuration, will use %s instead", MFISetsCheckConfiguration.DEFAULT.toString()));
		}
		
	}

	public void setPrintFormat(String string) {
		String[] input = string.trim().split(",");
		if (input[0]==null){
			System.err.println("The chosen format for block printing is unsupported.");
		}
		else {
			if (input[0].equalsIgnoreCase("B") || input[0].equalsIgnoreCase("N") || input[0].equalsIgnoreCase("S"))
				printFormat=input[0];
			else 
				System.err.println("The chosen format for block printing is unsupported.");
			if (input.length>1){
			originalRecordsPath=input[1];
			}
		}
	}

	public String getPrntFormat() {
		return printFormat;
	}
	public String getOriginalRecordsPath() {
		return originalRecordsPath;
	}
	
//	public void setRecords(Object object) {
//		recordSet= new RecordSet();
//		ArrayList<Record> temp=(ArrayList<Record>)object;
//		Map<Integer,Record> newMap=new HashMap<Integer, Record>();
//		for (int i=0; i< temp.size();i++){
//			newMap.put(temp.get(i).getId(), temp.get(i));
//		}
//		recordSet.setRecords(newMap);
//		
//	}

	public void setMatches(Object object) {
		// TODO Auto-generated method stub
		
	}

	public void setPropertiesFile(Object object) {
		propertiesFile= (String) object;
		
	}
	public String getPropertiesFile() {
		return propertiesFile;
		
	}

	public void setQgram(Object object) {
		qgramSize=(int)object;
		
	}

	public void setPruning(Object object) {
		pruning=(double)object;
		
	}

	public void setDatasetType(Object object) {
		datasetType=(String)object;
		
	}

//	public void setColumns(Object object) {
//		recordSet.setColumnNames((String[]) object);
//		
//	}

	public void setNumericFileName(String name) {
		numericFileName=name;
		
	}
	
	public String getNumericFileName() {
		return numericFileName;
		
	}

	public void setCleanStringFileName(String name) {
		cleanStringFileName=name;
		
	}

//	public void setRecordsFromStr(Map<Integer,String[]> records) {
//		recordSet= new RecordSet();
//		recordSet.setRecordsFromStr(records);
//		
//	}
//
//	public void setRecordset() {
//		RecordSet temp=new RecordSet();
//		temp.readRecords(this);
//		recordSet=temp;
//	}
	
	

}
