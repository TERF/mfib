//package mfib.core;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import mfib.bitsets.SingleBSFactory;
//import mfib.core.LexiconCSV.AttributeItems;
//
//
//import au.com.bytecode.opencsv.CSVReader;
//
//
//public class LexiconGenerator {
//	
//	private LexiconCSV lexicon;
//	private static final String CLUSTER_ATT_NAME = "class";
//	private static final String SOURCE_ATT_NAME = "source";
//	MFIBContext context; 
//	private WordProcessor wordProcessor;	
//	private BufferedWriter numericOutputWriter;
//	private BufferedWriter stringOutputWriter;
//	
//	public LexiconGenerator(MFIBContext context){
//		this.context=context;
//	}
//	
//	public LexiconCSV generate() throws IOException{
//		lexicon = new LexiconCSV(new File(context.getPropertiesFile()), context.recordSet().size);
//		wordProcessor = new WordProcessor(context.qgramSize,context.qgramSize);
//		numericOutputWriter = new BufferedWriter(new FileWriter(createOutputNumericFile()));
//		stringOutputWriter = new BufferedWriter(new FileWriter(createOutputCleanStringFile()));
//		removeTooFrequentItems();
//		for (int recordID=0;recordID< context.recordSet().size;recordID++){
//			
//			String[] parts = context.recordSet().values.get(recordID).recordStr();
//			int clusterAttIndex = getClusterFieldIndex(context.recordSet().schema());
//			int sourceAttIndex = getSourceFieldIndex(context.recordSet().schema());
//			StringBuilder numericStringBuilder = new StringBuilder();
//			StringBuilder cleanStringBuilder = new StringBuilder();	
//			for(int k=0 ; k < parts.length ; k++){	
//				if(k == clusterAttIndex|| k == sourceAttIndex){
//					continue; // do not want to write this as part of the file
//				}					
//				String toWrite = getCleanString(parts[k]);			
//				if(lexicon.getPrefixLengthForColumn(k) > 0){						
//					toWrite = toWrite.substring(0, 
//							Math.min(lexicon.getPrefixLengthForColumn(k),toWrite.length()));						
//				}		
//				
//				if(lexicon.getColumnWeight(k) > 0){
//					cleanStringBuilder.append(toWrite).append(" ");
//				}
//				
//				String ngramIdString = getNGramIdString(recordID, toWrite,k, true);
//				if(ngramIdString.trim().length() > 0){
//					numericStringBuilder.append(ngramIdString).append(" ");
//				}
//			
//			}
//			String numericString = numericStringBuilder.toString().trim();
//			numericOutputWriter.write(numericString.trim());
//			numericOutputWriter.newLine();
//			stringOutputWriter.write(cleanStringBuilder.toString().trim());
//			stringOutputWriter.newLine();
//		}
//		numericOutputWriter.close();
//        stringOutputWriter.close();        
//        //lexicon.exportToPropFile(LexiconOriginalOutFile);
//		return lexicon;
//	}
//	
//	public Map<Integer, FrequentItem> getItemsFromLexicon(){
//		Map<Integer, FrequentItem> result = new HashMap<Integer, FrequentItem>();
//		
//		Map<Integer,AttributeItems> attIdToItems = lexicon.getAttIdToItems();
//		for (AttributeItems ais : attIdToItems.values()) {
//			Map<Integer,FrequentItem> items = ais.getItems();
//			result.putAll(items);
//		}		
//		return result;
//	}
//	
//	private File createOutputCleanStringFile() {
//		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm");
//		Date date = new Date();
//		String dateFormated = dateFormat.format(date);
//		String workingDir = System.getProperty("user.dir");
//		File file = new File(workingDir + "/clean_" + dateFormated + ".txt");
//		context.setCleanStringFileName(file.getName());
//		return file;
//	}
//
//	private String getNGramIdString(int recordId, String value, int propertyId, boolean removedFrequent){
//		List<Integer> NGramsIds = new ArrayList<Integer>();
//		String[] values = value.split("\\s+");
//		for(int i=0; i < values.length ;i++){			
//			List<String> valueNgrams = getNGrams(values[i]);
//			NGramsIds.addAll(getNGramIds(valueNgrams,recordId,propertyId, removedFrequent)); //generic id
//		}
//		StringBuilder sb = new StringBuilder();
//		for (Integer ngramId : NGramsIds) {
//			sb.append(ngramId).append(" ");
//		}
//		return sb.toString().trim();		
//		
//	}
//	
//	private List<Integer> getNGramIds(List<String> nGrams, int recordId, int attributeId, boolean removedFrequent){		
//		List<Integer> retVal = new ArrayList<Integer>(nGrams.size());		
//		for (String nGram : nGrams) {
//			int wordId = 0;
//			if(!removedFrequent)
//				wordId = lexicon.addWord(attributeId, recordId, nGram);
//			else{
//				//check if ngram falls in expected support range
//				wordId = lexicon.wordExists(attributeId, nGram);
//			}
//			if(wordId >= 0 ){
//				retVal.add(wordId);
//			}
//		}
//		return retVal;
//	}
//	
//	private List<String> getNGrams(String value){
//		return wordProcessor.processValue(value);
//	}
//	
//	private static int sourceFieldIndex = -1;
//	private static int getSourceFieldIndex(String[] atts){
//		if(sourceFieldIndex > 0){
//			return sourceFieldIndex;
//		}		
//		for (int i=0 ; i < atts.length ; i++) {
//			if(atts[i].equals(SOURCE_ATT_NAME)){
//				sourceFieldIndex = i;
//				break;
//			}
//		}
//		return sourceFieldIndex;
//		
//	}
//	
//	private static int clusterFieldIndex = -1;
//	private static int getClusterFieldIndex(String[] atts){
//		if(clusterFieldIndex > 0){
//			return clusterFieldIndex;
//		}		
//		for (int i=0 ; i < atts.length ; i++) {
//			if(atts[i].equals(CLUSTER_ATT_NAME)){
//				clusterFieldIndex = i;
//				break;
//			}
//		}
//		return clusterFieldIndex;
//		
//	}
//	
//	private String getCleanString(String value){
//		List<String> swFreeTerms = wordProcessor.removeStopwordsAndSpecialChars(value);
//		return WordProcessor.concatListMembers(swFreeTerms);
//	}
//	
//	public File createOutputNumericFile() {
//		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm");
//		Date date = new Date();
//		String dateFormated = dateFormat.format(date);
//		String workingDir = System.getProperty("user.dir");
//		File file = new File(workingDir + "/numeric_" + dateFormated + ".txt");
//		context.setNumericFileName(file.getName());
//		return file;
//	}
//	
//	private void removeTooFrequentItems(){
//		
//		for (int recordID =0; recordID < context.recordSet().size(); recordID++){
//			RecordSet temp1 = context.recordSet();
//			Map<Integer, Record> temp2 = temp1.values();
//			Record temp3 = temp2.get(recordID);
//			
//			String[] parts = context.recordSet().values().get(recordID).recordStr();
//			int clusterAttIndex = getClusterFieldIndex(context.recordSet().schema());
//			int sourceAttIndex = getSourceFieldIndex(context.recordSet().schema());
//			StringBuilder numericStringBuilder = new StringBuilder();
//			StringBuilder cleanStringBuilder = new StringBuilder();	
//			for(int i=0 ; i < parts.length ; i++){	
//				if(i == clusterAttIndex|| i == sourceAttIndex){
//					continue; // do not want to write this as part of the file
//				}					
//				if(lexicon.getColumnWeight(i) <= 0){
//					continue;
//				}
//				String toWrite = getCleanString(parts[i]);	
//				if(lexicon.getPrefixLengthForColumn(i) > 0){						
//					toWrite = toWrite.substring(0, 
//							Math.min(lexicon.getPrefixLengthForColumn(i),toWrite.length()));						
//				}		
//				getNGramIdString(recordID, toWrite,i, false);				
//			}
//
//		}
//		lexicon.removeFrequentItems(context.recordSet().size(), context.getBestThreshold());
//
//	}
//}
