package mfib.core;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

public class SparkContext {
	
	public static void createSparkContext() {
		
			String operatingSystem = System.getProperty("os.name");
			boolean isWindows = operatingSystem.toLowerCase().contains("windows");
			if (isWindows){
				//System.setProperty("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
				//System.setProperty("spark.kryo.registrator", "fimEntityResolution.MyRegistrator");
				System.setProperty("spark.executor.memory", "5g");
				System.setProperty("hadoop.home.dir", "C:\\workspace\\winutils\\");
				//System.getProperty("spark.akka.askTimeout","50000");
				
			}
			else {//for linux
				//System.setProperty("spark.executor.memory", "3g");
				//System.setProperty("spark.driver.memory", "30g");
				//System.setProperty("spark.network.timeout", "500000"); 
				//System.setProperty("spark.driver.maxResultSize", "15g");
				
				
			}
			System.out.println("spark.executor.memory="+System.getProperty("spark.executor.memory"));
			System.out.println("spark.driver.memory="+System.getProperty("spark.driver.memory"));
			System.out.println("spark.network.timeout="+System.getProperty("spark.network.timeout"));
			System.out.println("spark.driver.maxResultSize="+System.getProperty("spark.driver.maxResultSize"));
			
			
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
			int numOfCores = runtime.availableProcessors();		
			SparkConf conf = new SparkConf();
			conf.setMaster("local["+numOfCores+"]");
			//conf.setMaster("local["+1+"]");
			conf.setAppName("MFIBlocks");
			//conf.set("spark.storage.memoryFraction", "0.5");
			MFIBContext.sc=new JavaSparkContext(conf);
			System.out.println("SPARK HOME="+MFIBContext.sc.getSparkHome());
	}
}
