package mfib.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.transaction.NotSupportedException;
import mfib.bitsets.EWAH_BitSet;
import mfib.interfaces.BitSetIF;
import mfib.interfaces.IFRecord;
import mfib.pools.BitMatrixPool;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
//import org.apache.spark.mllib.fpm.FPGrowth;
//import org.apache.spark.mllib.fpm.FPGrowthModel;
//import org.apache.spark.mllib.fpm.FPGrowth.FreqItemset;
import scala.Tuple2;
import com.google.common.base.Joiner;
import candidateMatches.CandidatePairs;
/***
 * Commented some parts in order to reduce memory consumption
 * @author Jonathan Svirsky
 */
public class SparkBlocksHandler {
	public static String NEW_LINE = System.getProperty("line.separator");
	static CandidatePairs candidatePairs;
	
	public static CandidatePairs getCandidatePairs(
			int recordsCardinality, 
			String recordsFile, 
			FrequentItemsetContext itemsetContext){
//		int minSup = itemsetContext.getMinimumSupport();
//		Runtime runtime = Runtime.getRuntime();
//		runtime.gc();
//		int numOfCores = runtime.availableProcessors();
//		long start=0;
//
//		//run pfpgrowth twice and get maximal FIs
//		start = System.currentTimeMillis();
//		JavaRDD<String> records=MFIBContext.sc.textFile(recordsFile,numOfCores*3).cache();
//		JavaRDD<List<Integer>> transactions =records.filter(new Function<String, Boolean>() {
//			@Override
//			public Boolean call(String v1) throws Exception {
//				if (v1.equalsIgnoreCase("")) return false;
//				return true;
//			}
//		}).map(new ParseRecordLine()).cache();
//		System.out.println("Starting PFPG - 1");
//		FPGrowth fpg = new FPGrowth().setMinSupport((double)minSup/recordsCardinality).setNumPartitions(numOfCores*3);	
//		FPGrowthModel<Integer> model = fpg.run(transactions);
//		System.out.println("PFPG - 1 finish");
//		JavaRDD<FreqItemset<Integer>> itemsets=model.freqItemsets().toJavaRDD().cache();
//		JavaRDD<List<Integer>> transactions2 = itemsets.map(new Function<FPGrowth.FreqItemset<Integer>, List<Integer>>(){	
//			@Override
//			public List<Integer> call(FreqItemset<Integer> v1) throws Exception {
//				return v1.javaItems();
//			}}).cache();
//		List<FreqItemset<Integer>> temp=itemsets.collect();
//		System.out.println("Starting PFPG - 2");
//		FPGrowth fpg2 = new FPGrowth().setMinSupport((double)1/temp.size()).setNumPartitions(numOfCores*3);
//		temp=null;
//		FPGrowthModel<Integer> model2 = fpg2.run(transactions2);
//		System.out.println("PFPG - 2 finish");
//		JavaRDD<FreqItemset<Integer>> FIs=model2.freqItemsets().toJavaRDD();
//		System.out.println("Time to run MFI with minsup="+minSup +
//				" on table of size " + (RecordSet.size-recordsCardinality) + 
//				" is " + Double.toString((double)(System.currentTimeMillis()-start)/1000.0));
//
//
//		//score the itemsets and produce candidate pairs
//		start = System.currentTimeMillis();
//		MFIBContext context = itemsetContext.getMfiContext();
//		double NG_PARAM = itemsetContext.getNeiborhoodGrowthLimit();
//		double scoreThreshold = itemsetContext.getMinBlockingThreshold();
//		Utilities.resetAtomicIntegerArr(Utilities.clusterScores);	
//		StringSimTools.numOfMFIs.set(0);
//		StringSimTools.numOfRoughMFIs.set(0);
//		StringSimTools.timeInGetClose.set(0);
//		StringSimTools.timeInRough.set(0);
//		BitMatrixPool.getInstance().restart();
//		int maxSize = (int) Math.floor(minSup * NG_PARAM );
//		candidatePairs = new CandidatePairs(maxSize);
//		JavaRDD<CandidateBlock> parsedBlocks = FIs.map(new FItoBlock());
//		JavaPairRDD<CandidateBlock,Double> blocksWithScores = parsedBlocks.mapToPair( new CalculateScores(context.getLexiconFile(),
//				context.getRecordsFile(), context.getOriginalFile(),scoreThreshold , minSup, NG_PARAM));
//		JavaPairRDD<CandidateBlock,Double> trueBlocks=blocksWithScores.filter(new TrueBlocks());
//		trueBlocks.count(); //JS:there is no need for the result, but we have to perform ACTION in order to apply our mapping
//		//and make distributed calculations in Spark
//		System.out.println("Time to read MFIs: " + Double.toString((double)(System.currentTimeMillis() - start)/1000.0) + " seconds");



		return candidatePairs;
		
	}
	
	
//	public static File RunPFPGrowth(int minSup, int recordsCardinality, String recordsFile, File MFIDir) {
//		System.out.println("free mem before activating FPMax: "	+ Runtime.getRuntime().freeMemory());
//		File file = null;
//		if (!MFIDir.exists()) {
//			if ( !MFIDir.mkdir() ) {
//				System.err.println("Directory " + MFIDir.getAbsolutePath()
//						+ " doesn't exist and failed to create it");
//			}
//		}
//		try {
//			file = File.createTempFile("MFIs", null, MFIDir);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		file.deleteOnExit();
//		System.out.println("recordsFile= " + recordsFile);
//		Runtime runtime = Runtime.getRuntime();
//		runtime.gc();
//		int numOfCores = runtime.availableProcessors();
//		System.out.println("Number of cores= " + numOfCores);
//		JavaRDD<String> records=MFIBContext.sc.textFile(recordsFile,numOfCores*3).cache();
//		System.out.println("File loaded");
//		JavaRDD<List<Integer>> transactions =records.filter(new Function<String, Boolean>() {
//			
//			@Override
//			public Boolean call(String v1) throws Exception {
//				if (v1.equalsIgnoreCase("")) return false;
//				return true;
//			}
//		}).map(new ParseRecordLine()).cache();
//		System.out.println("Transaction extracted");
//		System.out.println("Starting PFPG - 1");
//		FPGrowth fpg = new FPGrowth().setMinSupport((double)minSup/recordsCardinality).setNumPartitions(numOfCores*3);	
//		FPGrowthModel<Integer> model = fpg.run(transactions);
//		System.out.println("PFPG - 1 finish");
//		JavaRDD<FreqItemset<Integer>> itemsets=model.freqItemsets().toJavaRDD().cache();
//		
//		JavaRDD<List<Integer>> transactions2 = itemsets.map(new Function<FPGrowth.FreqItemset<Integer>, List<Integer>>(){
//			
//			@Override
//			public List<Integer> call(FreqItemset<Integer> v1) throws Exception {
//				return v1.javaItems();
//			}}).cache();
//		
//		List<FreqItemset<Integer>> temp=itemsets.collect();
//		System.out.println("Starting PFPG - 2");
//		
//		FPGrowth fpg2 = new FPGrowth().setMinSupport((double)1/temp.size()).setNumPartitions(numOfCores*3);
//		temp=null;
//		FPGrowthModel<Integer> model2 = fpg2.run(transactions2);
//		System.out.println("PFPG - 2 finish");
//		JavaRDD<FreqItemset<Integer>> FIs=model2.freqItemsets().toJavaRDD();
//		//FIs.saveAsTextFile(path)
//		List<FreqItemset<Integer>> itemsetsList=FIs.collect();
//		System.out.println("Final FIs list size = " + itemsetsList.size());
//		try{
//			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
//			for (FreqItemset<Integer> itemset : itemsetsList) {
//				if (itemset.freq()==1) {
//					StringBuilder sb=new StringBuilder();		
//					sb.append(Joiner.on(" ").join(itemset.javaItems()));
//					sb.append("  (");
//					sb.append(itemset.freq());
//					sb.append(")");		
//					writer.write(sb.toString());
//					writer.newLine();
//				}
//				//System.out.println(Joiner.on(" ").join(itemset.javaItems()) + "  (" + itemset.freq()+")")
//			}
//			writer.flush();
//			writer.close();
//		}
//		catch (IOException e){
//			System.out.println(e.getMessage().toString());
//		}
//		System.out.println("Finished PFPGrowth");
//		return file;
//	}

//	static class FItoBlock implements Function<FreqItemset<Integer>, CandidateBlock> {
//		public CandidateBlock call(FreqItemset<Integer> fi) {
//			if (fi == null)
//				return null;
//			List<Integer> retVal = new ArrayList<Integer>();
//			retVal.addAll(fi.javaItems());
//			int supportSize = (int)fi.freq();
//			CandidateBlock pFI=new CandidateBlock(retVal, supportSize);
//			return pFI;
//		}
//	}
	
	static class ParseRecordLine implements Function<String, List<Integer>> {
		public List<Integer> call(String line) {
			if (line == null)
				return null;
			List<Integer> retVal=new ArrayList<Integer>();
			line = line.trim();
			String[] items=line.split(" ");
			for (String item : items){
				retVal.add(Integer.parseInt(item));
			}
			return retVal;
		}
	}
	/**
	 * Imported from Utilities.resetAtomicIntegerArr
	 * @param arr
	 */
	private static void resetAtomicIntegerArr(AtomicInteger[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = new AtomicInteger(0);
		}
	}
	/**
	 * TrueBlocks extends Function<Tuple2<CandidateBlock, Double>, Boolean>
	 * for filtering blocks that holds our 3 conditions.
	 * -100.0: tooLarge in original	
	 * -200.0: scorePruned
	 * -300.0: nonFIs (support.getCardinality() <  minSup) 
	 * -400.0: not classified
	 *
	 */
	static class TrueBlocks implements Function<Tuple2<CandidateBlock, Double>, Boolean>{

		@Override
		public Boolean call(Tuple2<CandidateBlock, Double> tuple)
				throws Exception {
			if (tuple._2!=-100.0 && tuple._2!=-200.0 && tuple._2!=-300.0 &&tuple._2!=-400.0)
				return true;
			return false;
		}	
	}

	static class CalculateScores implements PairFunction<CandidateBlock, CandidateBlock, Double>{

		double scoreThreshold;
		int minSup;
		double NG_PARAM;

		public CalculateScores(String lexiconFile, String recordsFile,String origRecordsFile,
				double scoreThreshold, int minSup, double nG_PARAM){
			this.scoreThreshold=scoreThreshold;
			this.minSup=minSup;
			this.NG_PARAM=nG_PARAM;
			
		}
		@Override
		public Tuple2<CandidateBlock, Double> call(CandidateBlock candidateBlock)
				throws Exception {
			// 3.1 support condition 
			if (candidateBlock.supportSize > minSup * NG_PARAM) {
				//tooLarge++;
				return new Tuple2<CandidateBlock, Double>(candidateBlock, -100.0);
			}

			// 3.2 ClusterJaccard score
			List<Integer> currentItemSet = candidateBlock.items;
			double maxClusterScore = StringSimTools.MaxScore(candidateBlock.supportSize, currentItemSet, RecordSet.minRecordLength);
			if (maxClusterScore < 0.1 * scoreThreshold) {
				//scorePruned++;
				return new Tuple2<CandidateBlock, Double>(candidateBlock, -200.0);
			}
			// 3.3 sparse neighborhood condition
			BitSetIF support = null;
			support = getItemsetSupport(currentItemSet);
			if (support.getCardinality() <  minSup) {
				//SparkBlocksReader.nonFIs.incrementAndGet();
				return new Tuple2<CandidateBlock, Double>(candidateBlock, -300.0); // must be the case that the item appears minSup times
				// but in a number of records < minsup
			}
			
			List<IFRecord> FISupportRecords = support.getRecords();
			double currClusterScore = StringSimTools.softTFIDF(
					FISupportRecords, currentItemSet, scoreThreshold);
			FISupportRecords = null;
			Utilities.clusterScores[cellForCluster(currClusterScore)].incrementAndGet();
			if (currClusterScore > scoreThreshold) {
				//SparkBlocksReader.numOfFIs.incrementAndGet();
				support.markPairs(candidatePairs,currClusterScore,candidateBlock.items);
				return new Tuple2<CandidateBlock, Double>(candidateBlock, currClusterScore);
			}
			//JS:Non of conditions passed and wasn't classified.
			return new Tuple2<CandidateBlock, Double>(candidateBlock, -400.0);
		}
		/**
		 * Utilities.getItemsetSupport
		 * @param items
		 * @return
		 */
		public static BitSetIF getItemsetSupport(List<Integer> items) {
			try {
				long start = System.currentTimeMillis();
				BitSetIF retVal = new EWAH_BitSet();
				try {
					retVal = retVal.or(Utilities.globalItemsMap.get(items.get(0))
							.getSupport());
					for (int i = 1; i < items.size(); i++) {
						int item = items.get(i);
						BitSetIF itemSupport = Utilities.globalItemsMap.get(item)
								.getSupport();
						retVal = retVal.and(itemSupport);
					}
				} catch (NotSupportedException e) {
					e.printStackTrace();
					return null;
				}
				if (retVal.getCardinality() <= 0) {
					System.out
							.println("DEBUG: itemset with support retVal.getCardinality()"
									+ retVal.getCardinality());
				}
				//time_in_supp_calc.addAndGet(System.currentTimeMillis() - start);
				return retVal;
			} catch (Exception e) {
				System.out.println("cought exception " + e);
				e.printStackTrace();
				return null;
			}

		}
		/**
		 * Imported from Utilities.cellForCluster
		 * @param score
		 * @return
		 */
		private static int cellForCluster(final double score) {
			return (int) Math.ceil(score / 0.05);
		}
	}
}
