package mfib.enums;

public enum DatasetName {
	CORA, CENSUS, CDDB, MOVIES, RESTS, DBPEDIA, OTHER, FEBRL
}
