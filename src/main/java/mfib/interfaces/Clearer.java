package mfib.interfaces;

import java.io.Serializable;

public interface Clearer extends Serializable {
	public void clearAll();
	
}
