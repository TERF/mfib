package mfib.interfaces;

public interface BitSetFactory extends ClearerFactory{

	public BitSetIF createInstance();
}
