package mfib.external;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import mfib.enums.DataFormat;

/***
 * This interface provides some comments for acceptable MFIB arguments during
 * external usage (as a jar lib). It addes an option for usinf MFIB as an incremental
 * clustering algorithm.
 * 
 * @author Jonathan Svirsky
 */
public abstract class AbstractMFIB {
	
	/***
	 * Here you should run the algorithm by next steps:
	 * 1. creating an object MFIBMain
	 * 2. calling setConfiguration(arguments) method;
	 * 3. calling run() method that returns Map<String,List<Integer>> matches
	 * @param arguments
	 * 1. String (dataset type) : CORA, CENSUS, CDDB, MOVIES, RESTS, DBPEDIA, OTHER, FEBRL (enums.DatasetType.java)
	 * 2. Map<Integer, Record> (records)
	 * 3. String[] (columns)
	 * 4. Map<String,List<Integer>> (matches)
	 * 5. String (properties.file path)
	 * 6. int (qgram size)
	 * 7. double (pruning threshold)
	 * @return matches
	 */
	public abstract void run();
	/***
	 * Transform MFIB results to desired format
	 * @return
	 */
	public abstract Object transform();
	
	
}
