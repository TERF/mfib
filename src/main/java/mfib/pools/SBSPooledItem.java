package mfib.pools;

import mfib.interfaces.Clearer;

import org.enerj.core.SparseBitSet;


public class SBSPooledItem implements Clearer{
	private SparseBitSet sbs;
	public SBSPooledItem(){
		sbs = new SparseBitSet();
	}

	@Override
	public void clearAll() {
		sbs.clear();
	}

}


