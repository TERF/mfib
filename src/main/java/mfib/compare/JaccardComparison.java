package mfib.compare;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import mfib.core.BottomUp;
import mfib.core.RecordSet;
import candidateMatches.CandidatePairs;
import candidateMatches.RecordMatches;


public class JaccardComparison {
	
	private Map<Integer, Integer> cache;
	private int[][] compared;
	
	
	public  JaccardComparison(){
		this.cache = new HashMap<Integer, Integer>();
		compared = new int[RecordSet.size+1][RecordSet.size+1];
	}
	
	/***
	 * Measure pairs comparison execution on the produced blocks.
	 * Jaccard similarity is calculated by finding intersection and unioun between record' qgrams.
	 * @param candidatePairs
	 * @return time in seconds
	 */
	public long measureComparisonExecution(final CandidatePairs candidatePairs) {
		long startingTime = System.currentTimeMillis();
		for (Entry<Integer, RecordMatches> entry : candidatePairs.getAllMatchedEntries()){
			Set<Integer> comparisonSet = new HashSet<Integer>();
			comparisonSet.add(entry.getKey());
			final Set<Integer> others = entry.getValue().getMatchedIds();
			comparisonSet.addAll(others);
			//int[][] compared = new int[comparisonSet.size()][comparisonSet.size()];
			for (Integer id1: comparisonSet){
				for (Integer id2: comparisonSet){
					if (compared[id1][id2]==1 || compared[id2][id1]==1 || id1==id2) continue;
					//if (id1==id2) continue;
					//if (isCompared(id1, id2)) continue;
					final Set<Integer> attributes1=RecordSet.values.get(id1).getItemsToFrequency().keySet();
					final Set<Integer> attributes2=RecordSet.values.get(id2).getItemsToFrequency().keySet();
					//JS: can be extended to return the actual comparison value from getJaccardSimilarity
					ProfileComparison.getJaccardSimilarity(attributes1, attributes2);
					//addToCache(id1, id2);
					compared[id1][id2]=1; compared[id2][id1]=1;
				}
			}
		}
		long endTime = System.currentTimeMillis();
		return endTime - startingTime;
	}

	private void addToCache(Integer valueOne, Integer valueTwo) {
		cache.put(valueOne, valueTwo);
		cache.put(valueTwo, valueOne);
	}
	
	private boolean isCompared(Integer valueOne, Integer valueTwo) {
		Integer actuallValueTwo = cache.get(valueOne);
		if (actuallValueTwo != null) {
			return actuallValueTwo.equals(valueTwo);
		}
		return false;
	}
}
