package mfib.compare;

import candidateMatches.CandidatePairs;

public interface IComparison {
	public long measureComparisonExecution(
			CandidatePairs algorithmObtainedPairs);
}
