package candidateMatches;

import java.io.Serializable;

public interface PositionUpdater extends Serializable{

	public void setHeapPos(int pos);
}
