package experiments;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.NotSupportedException;

import mfib.bitsets.EWAH_BitSet;
import mfib.core.CandidateBlock;
import mfib.core.MFIBContext;
import mfib.core.TrueClusters;
import mfib.core.Utilities;
import mfib.core.BottomUp.MFISetsCheckConfiguration;
import mfib.core.SparkContext;
import mfib.interfaces.BitSetIF;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import candidateMatches.CandidatePairs;

public class Movies {
	//load lexicons
	//merge lexicons by string value
	//load blocks 1
	//load blocks 2
	//merge them
	//update candidatePairs
	//evaluate
	static CandidatePairs candidatePairs;
	
	public static void main(String[] args){
		String blocksPath1=args[0];
		String blocksPath2=args[1];
		int firstDSSize=Integer.parseInt(args[2]);
		SparkContext.createSparkContext();
		
		mergeBlocks(blocksPath1, blocksPath2, firstDSSize);
//		long actionStart = System.currentTimeMillis();
//		writeCandidatePairs(algorithmObtainedPairs,context);
//		long writeBlocksDuration = System.currentTimeMillis() - actionStart;
//		
//		actionStart = System.currentTimeMillis();
		TrueClusters trueClusters = new TrueClusters();
//		trueClusters.findClustersAssingments(context.getMatchFile());
//		//System.out.println("DEBUG: Size of trueClusters: " + MemoryUtil.deepMemoryUsageOf(trueClusters, VisibilityFilter.ALL)/Math.pow(2,30) + " GB");
//		
//		ExperimentResult experimentResult = new ExperimentResult(trueClusters, algorithmObtainedPairs, recordsSize);
//		StatisticMeasuremnts results = experimentResult.calculate();
//		long totalMaxRecallCalculationDuration = System.currentTimeMillis() - actionStart;
//		long timeOfERComparison =1;
//		//long timeOfERComparison = pairsComparison.measureComparisonExecution(algorithmObtainedPairs);
//		double executionTime = calcExecutionTime(start, totalMaxRecallCalculationDuration, writeBlocksDuration);
//		BlockingResultContext resultContext = new BlockingResultContext(results, minBlockingThreshold, lastUsedBlockingThreshold, NG_LIMIT, 
//				executionTime, Utilities.convertToSeconds(timeOfERComparison));
//		BlockingRunResult blockingRR = new BlockingRunResult(resultContext);
//		blockingRunResults.add(blockingRR);
//		
//		System.out.println("");
//		System.out.println("");
	}

	private static void mergeBlocks(String path1, String path2,int size) {
		JavaPairRDD<List<Integer>, CandidateBlock> blocks1 = MFIBContext.sc.objectFile(path1).mapToPair(new ObjectToPair());
		JavaPairRDD<List<Integer>, CandidateBlock> blocks2 = MFIBContext.sc.objectFile(path2).mapToPair(new ObjectToPair());
		JavaPairRDD<List<Integer>,Tuple2<Iterable<CandidateBlock>,Iterable<CandidateBlock>>> temp=blocks1.cogroup(blocks2);
		JavaRDD<CandidateBlock> finalBlocks = temp.map(new Merge(size));
		
	}
	
	
	
	static class ObjectToPair implements PairFunction<Object,List<Integer>, CandidateBlock>{

		@Override
		public Tuple2<List<Integer>, CandidateBlock> call(Object object)
				throws Exception {
			// TODO Auto-generated method stub
			return (Tuple2<List<Integer>, CandidateBlock>)object;
		}
	}
	
	static class Merge implements Function<Tuple2<List<Integer>,Tuple2<Iterable<CandidateBlock>,Iterable<CandidateBlock>>>, CandidateBlock>{ 
		int size=-1;
		public Merge (int size){
			this.size=size;
		}
		
		@Override
		public CandidateBlock call(Tuple2<List<Integer>, Tuple2<Iterable<CandidateBlock>, Iterable<CandidateBlock>>> tuple)
				throws Exception {
			CandidateBlock result= new CandidateBlock(new ArrayList<Integer>(), -1);
			for (CandidateBlock cb : tuple._2._1){
				result.addSupport(cb.supportRecords);
			}
			for (CandidateBlock cb : tuple._2._2){
				result.addSupport(cb.supportRecords);
			}
			BitSetIF support = null;
			support = getItemsetSupport(tuple._1);
			support.markPairs(candidatePairs,1.0,tuple._1);
			return result;
		}
	}
	
	public static BitSetIF getItemsetSupport(List<Integer> items) {
		try {
			long start = System.currentTimeMillis();
			BitSetIF retVal = new EWAH_BitSet();
			try {
				retVal = retVal.or(Utilities.globalItemsMap.get(items.get(0))
						.getSupport());
				for (int i = 1; i < items.size(); i++) {
					int item = items.get(i);
					BitSetIF itemSupport = Utilities.globalItemsMap.get(item)
							.getSupport();
					retVal = retVal.and(itemSupport);
				}
			} catch (NotSupportedException e) {
				e.printStackTrace();
				return null;
			}
			if (retVal.getCardinality() <= 0) {
				System.out
						.println("DEBUG: itemset with support retVal.getCardinality()"
								+ retVal.getCardinality());
			}
			//time_in_supp_calc.addAndGet(System.currentTimeMillis() - start);
			return retVal;
		} catch (Exception e) {
			System.out.println("cought exception " + e);
			e.printStackTrace();
			return null;
		}

	}
	
	
}
